<?php
	require("validarprofe.php");
	require("../conectar.php");

	$user=$_SESSION['user'];	
	$sql="SELECT * from usuarios WHERE usuario='$user'";
	$ejecuta=mysqli_query($conexion, $sql);
	while ($datos=mysqli_fetch_array($ejecuta)){
		$idusuario=$datos['idusuario'];

		}
		$hoy=date("Y-m-d");
 	//fecha
	$sql="SELECT * FROM periodos WHERE fechai<='$hoy' and fechaf>='$hoy'";
	$ejecuta=mysqli_query($conexion, $sql);
	while ($datos=mysqli_fetch_array($ejecuta)) {
		$idperiodo=$datos['idperiodo'];
		$periodo=$datos['periodo'];		
	}	

	$sql1="SELECT * from profesores WHERE idusuario='$idusuario'";
	$ejecuta1=mysqli_query($conexion, $sql1);
	while ($datos1=mysqli_fetch_array($ejecuta1)){
		$idprofesor=$datos1['idprof'];
		$idcarrera=$datos1['idcarrera'];
		}
		
	$sql2="SELECT * from carreras WHERE idcarrera='$idcarrera'";
	$ejecuta2=mysqli_query($conexion, $sql2);
	while ($datos2=mysqli_fetch_array($ejecuta2)){
		$carrera=$datos2['carrera'];
		}
?>

<html>
<head>
	<title>Grupos</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<meta charset="utf-8">
	<style type="text/css">
	<style>
	div{
		width: 100%;
	}
table {

    border-collapse: collapse;
    text-align: center;
    width: 100%;
}

th, td {
    text-align: center;
    padding: 10px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: #4CAF50;
    color: white;
}
a{
			text-decoration: none;
			color: green;
		}
	.ok{

		border: 2px solid green;
		background-color: green;
		border-radius: 15px;
		color: white;
		margin: auto;
		text-align: center;
		width: 49%;
	}
	.error{

		border: 2px solid red;
		background-color: red;
		border-radius: 15px;
		color: white;
		margin: auto;
		text-align: center;
		width: 49%;

	}
</style>		
</head>
<body>
<div class="headerlogopag">
	<img src="../img/logo.png">
</div>
<header>
	<nav class="menu">
		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesion </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			      <a href="materias.php">Ver mis grupos</a>
			      
			    </div>
			 </li>
			 <li>
				<a href="menuc.php">Inicio </a>
			</li>
		</ul>
	</nav>

</header>
<section class="contenido wrapper">
<div>
	<?php if(@$_GET["m"]=="correcto"){ ?>
				<div class="correcto">
					<p>Correcto!</p> 
				</div>
<?php } ?>

	<?php if(@$_GET["m"]=="error"){ ?>
				<th><div class="error">
							<p>Error</p> </th>
				<td>
						</div>
						
						<?php } ?>

	<?php if(@$_GET["m"]=="peso"){ ?>
				<th><div class="error">
							<p>El peso del archivo sobrepasa lo solicitado</p> </th>
				<td>
						</div>
						
						<?php } ?>
	<?php if(@$_GET["m"]=="formato"){ ?>
				<th><div class="error">
							<p>El archivo no esta en el formato solicitado</p> </th>
				<td>
						</div>
						
						<?php } ?>

<table border="1" cellspacing="0" rules="rows">
	<center>
	<tr> 
		<th>Grupo(s)</th>
		<th>División</th>
		<th>Materia</th>
	</tr>

<?php 

require('../conectar.php');
$sql="SELECT g.cvegrupo, m.materia, p.idmateria, p.idcarrera, c.carrera  
	FROM profegrupo p INNER JOIN materias m on p.idmateria=m.idmateria 
	inner join carreras c on p.idcarrera=c.idcarrera 
	INNER JOIN grupos g on p.idgrupo=g.idgrupo 
	WHERE p.idprof='$idprofesor' and p.idperiodo='$idperiodo'";
$ejecuta=mysqli_query($conexion, $sql);
while ($datos=mysqli_fetch_array($ejecuta)) {

	echo "<tr>";
	echo "<td>".$datos['0']."</td>";
	echo "<td>".$datos['carrera']."</td>";
	$idcarrera=$datos['idcarrera'];
	echo "<td><a href="."archivos.php?materia=$datos[2]&grupo=$datos[0]&profesor=$idprofesor&carrera=$idcarrera".">".$datos['1']."</a></td>";
	
	
	

	echo "</tr>";
	echo "<br>";

	
}



?>

</table>
</center>
</div>
</section>


</body>
</html>