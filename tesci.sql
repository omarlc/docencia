CREATE TABLE `actividades` (
  `idactividad` int(11) NOT NULL,
  `nombre` varchar(80) CHARACTER SET utf8 DEFAULT NULL,
  `tipo` varchar(50) CHARACTER SET utf8 DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `idperiodo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `carreras` (
  `idcarrera` int(11) NOT NULL,
  `cvecarrera` varchar(10) DEFAULT NULL,
  `carrera` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `grupos` (
  `idgrupo` int(11) NOT NULL,
  `cvegrupo` varchar(5) DEFAULT NULL,
  `idperiodo` int(11) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `jefes` (
  `idjefe` int(11) NOT NULL,
  `matricula` varchar(15) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apaterno` varchar(30) DEFAULT NULL,
  `amaterno` varchar(30) DEFAULT NULL,
  `sexo` varchar(10) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `materias` (
  `idmateria` int(11) NOT NULL,
  `cvemateria` varchar(15) DEFAULT NULL,
  `materia` varchar(60) DEFAULT NULL,
  `competencias` varchar(15) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL,
  `idsemestre` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `periodos` (
  `idperiodo` int(11) NOT NULL,
  `fechai` date NOT NULL,
  `fechaf` date NOT NULL,
  `periodo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `periodos` (`idperiodo`, `fechai`, `fechaf`, `periodo`) VALUES
(1, '2018-08-15', '2019-01-11', '2018-2');

CREATE TABLE `profact` (
  `idpa` int(11) NOT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idactividad` int(11) DEFAULT NULL,
  `idmateria` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idperiodo` int(11) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL,
  `estatus` varchar(20) DEFAULT NULL,
  `validar` varchar(20) DEFAULT NULL,
  `subido` varchar(30) DEFAULT NULL,
  `ruta` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profegrupo` (
  `idpg` int(11) NOT NULL,
  `idprof` int(11) DEFAULT NULL,
  `idgrupo` int(11) DEFAULT NULL,
  `idmateria` int(11) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL,
  `idperiodo` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `profesores` (
  `idprof` int(11) NOT NULL,
  `matricula` varchar(15) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apaterno` varchar(30) DEFAULT NULL,
  `amaterno` varchar(30) DEFAULT NULL,
  `sexo` varchar(10) DEFAULT NULL,
  `telefono` varchar(10) DEFAULT NULL,
  `idusuario` int(11) DEFAULT NULL,
  `idcarrera` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `semestres` (
  `idsemestre` int(11) NOT NULL,
  `semestre` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `semestres` (`idsemestre`, `semestre`) VALUES
(1, 'Primero'),
(2, 'Segundo'),
(3, 'Tercero'),
(4, 'Cuarto'),
(5, 'Quinto'),
(6, 'Sexto'),
(7, 'Septimo'),
(8, 'Octavo');

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `usuario` varchar(30) DEFAULT NULL,
  `password` varchar(16) DEFAULT NULL,
  `privilegios` varchar(10) DEFAULT NULL,
  `correo` varchar(35) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `usuarios` (`idusuario`, `usuario`, `password`, `privilegios`, `correo`) VALUES
(1, 'admin', '12345678', 'admin', '');

ALTER TABLE `actividades`
  ADD PRIMARY KEY (`idactividad`),
  ADD KEY `periodos` (`idperiodo`) USING BTREE;

ALTER TABLE `carreras`
  ADD PRIMARY KEY (`idcarrera`),
  ADD UNIQUE KEY `clave1` (`cvecarrera`),
  ADD UNIQUE KEY `carrera` (`carrera`);

ALTER TABLE `grupos`
  ADD PRIMARY KEY (`idgrupo`),
  ADD UNIQUE KEY `cvegrupo` (`cvegrupo`),
  ADD KEY `periodo` (`idperiodo`),
  ADD KEY `carrera` (`idcarrera`);

ALTER TABLE `jefes`
  ADD PRIMARY KEY (`idjefe`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD KEY `usuario` (`idusuario`) USING BTREE,
  ADD KEY `carrera` (`idcarrera`) USING BTREE;

ALTER TABLE `materias`
  ADD PRIMARY KEY (`idmateria`),
  ADD UNIQUE KEY `cvemateria` (`cvemateria`),
  ADD KEY `carr` (`idcarrera`),
  ADD KEY `semestre` (`idsemestre`);

ALTER TABLE `periodos`
  ADD PRIMARY KEY (`idperiodo`),
  ADD UNIQUE KEY `periodo` (`periodo`);

ALTER TABLE `profact`
  ADD PRIMARY KEY (`idpa`),
  ADD KEY `act` (`idactividad`),
  ADD KEY `materi` (`idmateria`),
  ADD KEY `profa` (`idprof`),
  ADD KEY `grupo` (`idgrupo`),
  ADD KEY `perio` (`idperiodo`),
  ADD KEY `carr1` (`idcarrera`);

ALTER TABLE `profegrupo`
  ADD PRIMARY KEY (`idpg`),
  ADD KEY `prof` (`idprof`) USING BTREE,
  ADD KEY `group` (`idgrupo`) USING BTREE,
  ADD KEY `mat` (`idmateria`),
  ADD KEY `ca` (`idcarrera`),
  ADD KEY `per` (`idperiodo`);

ALTER TABLE `profesores`
  ADD PRIMARY KEY (`idprof`),
  ADD UNIQUE KEY `matricula` (`matricula`),
  ADD KEY `usuarios` (`idusuario`) USING BTREE,
  ADD KEY `carreras` (`idcarrera`) USING BTREE;

ALTER TABLE `semestres`
  ADD PRIMARY KEY (`idsemestre`);

ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`),
  ADD UNIQUE KEY `usuario` (`usuario`),
  ADD UNIQUE KEY `correo` (`correo`);

ALTER TABLE `actividades`
  MODIFY `idactividad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `carreras`
  MODIFY `idcarrera` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `grupos`
  MODIFY `idgrupo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `jefes`
  MODIFY `idjefe` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `materias`
  MODIFY `idmateria` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `periodos`
  MODIFY `idperiodo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `profact`
  MODIFY `idpa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `profegrupo`
  MODIFY `idpg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `profesores`
  MODIFY `idprof` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

ALTER TABLE `semestres`
  MODIFY `idsemestre` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

ALTER TABLE `actividades`
  ADD CONSTRAINT `actividades_ibfk_1` FOREIGN KEY (`idperiodo`) REFERENCES `periodos` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `grupos`
  ADD CONSTRAINT `carrera` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `periodo` FOREIGN KEY (`idperiodo`) REFERENCES `periodos` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `jefes`
  ADD CONSTRAINT `jefes_ibfk_1` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `jefes_ibfk_2` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `materias`
  ADD CONSTRAINT `carr` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `semestre` FOREIGN KEY (`idsemestre`) REFERENCES `semestres` (`idsemestre`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `profact`
  ADD CONSTRAINT `act` FOREIGN KEY (`idactividad`) REFERENCES `actividades` (`idactividad`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `carr1` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `grupo` FOREIGN KEY (`idgrupo`) REFERENCES `grupos` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `materi` FOREIGN KEY (`idmateria`) REFERENCES `materias` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `perio` FOREIGN KEY (`idperiodo`) REFERENCES `periodos` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profa` FOREIGN KEY (`idprof`) REFERENCES `profesores` (`idprof`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `profegrupo`
  ADD CONSTRAINT `ca` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `group` FOREIGN KEY (`idgrupo`) REFERENCES `grupos` (`idgrupo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `mat` FOREIGN KEY (`idmateria`) REFERENCES `materias` (`idmateria`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `per` FOREIGN KEY (`idperiodo`) REFERENCES `periodos` (`idperiodo`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `prof` FOREIGN KEY (`idprof`) REFERENCES `profesores` (`idprof`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `profesores`
  ADD CONSTRAINT `profesores_ibfk_2` FOREIGN KEY (`idcarrera`) REFERENCES `carreras` (`idcarrera`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `profesores_ibfk_4` FOREIGN KEY (`idusuario`) REFERENCES `usuarios` (`idusuario`) ON DELETE CASCADE ON UPDATE CASCADE;
