<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/estilos.css">
  <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon">
  <title>Recuperar Contraseña</title>
</head>

<body>
  <div class="headerlogo">
    <img src="img/logo.png">
  </div>
  <form class="form" action="validarcorreo.php" method="POST">
    <label for="email">Correo electronico</label>
    <input type="email" name="email" id="email" minlength="8" autofocus required placeholder="Introduce tu correo electronico...">
    <input type="submit" value="Consultar" class="btn" name="consultar">
    <a href="index.php">Regresar al Inicio</a>
  </form>
</body>

</html>