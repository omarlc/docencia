<?php
	require ("../conectar.php");

	$nombre=($_GET['nombre_archivo']);
	$tipo=($_GET['tipo_archivo']);
	$fecha=($_GET['fecha_archivo']);

	$sql1="SELECT * from actividades WHERE nombre='$nombre'";
		$ejecuta1=mysqli_query($conexion, $sql1);
		while ($datos1=mysqli_fetch_array($ejecuta1)){
			$id_actividad=$datos1['idactividad'];
			}
?>

<html>
	<head>
		<title>Formulario</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../css/estilos.css">
		<link rel="icon" type="image/png" href="../img/tesci.ico">
		<style type="text/css">

			input[type=text], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
				}

			input[type=date], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
				}

			input[type=password], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
				}

			a{
					text-decoration: none;
					color: white;
				}

			a:hover{
				color: black;
			}

			.correcto{
				border-radius: 5px;
				background-color: green;
				color: white;
				margin: auto;
				height: 20px;
				width: 150px;
				}
		</style>
	</head>
	<body>
		<div class="headerlogopag">
			<img src="../img/logo.png">
		</div>

		<header>
			<nav class="menu">
				<ul>
					<li>
						<a href="../cerrarsesion.php"> Cerrar Sesión </a>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Actividades</a>
						<div class="dropdown-content">
						<a href="altaarchivos.php">Alta</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Grupos</a>
						<div class="dropdown-content">
						<a href="altagrupos.php">Alta</a>
						
						</div>
					</li>
					
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Materias</a>
						<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>
						
						</div>
					</li>
						<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Profesores</a>
						<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						
						</div>
					</li>	
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Jefes</a>
						<div class="dropdown-content">
						<a href="altajefe.php">Alta y cambios</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Periodos</a>
						<div class="dropdown-content">
						<a href="altaperiodo.php">Alta y cambios</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Divisiones</a>
						<div class="dropdown-content">
							<a href="altadivision.php">Alta y cambios</a>
						</div>
					</li>
					<li>
						<a href="menua.php">Inicio</a>
					</li>
				</ul>
			</nav>
		</header>
		<section class="contenido wrapper">
			<div class="formularios">
				<table border="0" align="center">
					<h1>Actualizar</h1>

					<form name="form1" id="form1" method="post" action="actualizaarchivo2.php">

						<tr>
							<th> Periodo: </th>
							<td>
								<select class="periodo" name="periodo" width="400px">
									<?php 
										require('../conectar.php');
										$sql="select * from periodos";
										$ejecuta=mysqli_query($conexion, $sql);
										while ($datos=mysqli_fetch_array($ejecuta)) {
											echo " <option value=\"".$datos[3]."\">".$datos[3]."</option>\n";
										}
									?>
								</select>	
							</td>			
						</tr>
						<tr>
							<th>Actividad: </th>
							<td><input type="text" name="nombre" required placeholder="Intoduce el nombre de la actividad..." value="<?php echo $nombre; ?>"></td>
						</tr>
						<tr>
							<th>Tipo:  <?php echo $tipo; ?></th>
							<td>
								<select name="tipo" requiered  placeholder="Selecciona el tipo de actividad..." value="<?php echo $tipo; ?>">
										<option>Inicio</option>
										<option>Seguimiento</option>
										<option >Final</option>
								</select>
							</td>
						</tr>
						<tr>
							<th>Fecha Entrega:</th>
							<td>
								<input type="date" name="fecha" minlength="1" required placeholder="..." value="<?php echo $fecha; ?>">
							</td>
						</tr>	
						<tr>
							<th></th>
							<td>
								<input type="text" name="id_actividad" style="visibility:hidden" id="id_actividad" value="<?php echo $id_actividad; ?>"><br>
							</td>
						</tr>
						<tr>
							<th><input type="submit" class="btn" value="Actualizar"></th>
							<td>
								<a href="altaarchivos.php">
									<input type="button" class="btn" value="Cancelar">
								</a>
							</td>
						</tr>
					</form>
				</table>
			</div>
		</section>
	</body>
</html>