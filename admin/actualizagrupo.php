<?php
$cvegrupo=($_GET['m']);
$idgrupo=($_GET['i']);

?>

<html>
<head>
	<title>Formulario</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<style type="text/css">

		input[type=text], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=password], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}

	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
</head>
<body>
	<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesión </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Alta</a>
			      
			    </div>
			 </li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta</a>
			      
			    </div>
			 </li>
			
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 	<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>
			      
			     </div>
			 </li>	
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Jefes</a>
   				 <div class="dropdown-content">
			      <a href="altajefe.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Periodos</a>
   				 <div class="dropdown-content">
			     <a href="altaperiodo.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Divisiones</a>
   				 <div class="dropdown-content">
			     <a href="altadivision.php">Alta y cambios</a>
			           
			    </div>
			 </li>
			  <li>
				<a href="menua.php">Inicio</a>
			</li>
		</ul>
	</nav>

</header>



<section class="contenido wrapper">
	<div class="formularios">
				<table border="0" align="center">
	<h1>Actualizar</h1>

		<form name="form1" id="form1" method="post" action="actualizagrupo2.php">

		<tr>
			<th>Grupo: </th>
			<td><input type="text" name="cvegrupo" id="cvegrupo" value="<?php echo $cvegrupo; ?>"><br></td>
		</tr>

		<tr>
			<th></th>
			<td><input type="text" name="idgrupo" style="visibility:hidden" id="idgrupo" value="<?php echo $idgrupo; ?>"><br></td>
		</tr>
		
		<tr>
			<th><input type="submit" class="btn" value="Actualizar"> </th>
		
		<td><a href="altagrupos.php">
		<input type="button" class="btn" value="Cancelar">
		</a></td>
		</tr>
		</form>
		</table>
	</div>
	</section>


</body>
</html>