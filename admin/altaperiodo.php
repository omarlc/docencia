<?php
require("validaradmin.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Alta de periodos</title>
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<meta charset="utf-8">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}


		input[type=date],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}

		a {
			text-decoration: none;
			color: white;
		}

		a:hover {
			color: black;
		}

		.correcto {
			border-radius: 5px;
			background-color: green;
			color: white;
			margin: auto;
			height: 20px;
			width: 150px;
		}

		.error {
			border-radius: 5px;
			background-color: red;
			color: white;
			margin: auto;
			height: 20px;
			width: 120px;
		}
	</style>
	<script type="text/javascript">
		function confirmar(idperiodo, periodo) {
			if (confirm("Deseas elimnar el periodo: " + periodo + "?")) {
				window.location.href = "eliminarperiodo.php?idperiodo=" + idperiodo;
			}
		}
	</script>
</head>

<body>

	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesión </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Alta</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta</a>

					</div>
				</li>

				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Jefes</a>
					<div class="dropdown-content">
						<a href="altajefe.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Periodos</a>
					<div class="dropdown-content">
						<a href="altaperiodo.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Divisiones</a>
					<div class="dropdown-content">
						<a href="altadivision.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menua.php">Inicio</a>
				</li>
			</ul>
		</nav>

	</header>

	<section class="contenido wrapper">
		<div class="formularios">
			<form name="form" id="from" action="conealtaperiodo.php" method="post">
				<table border="0" align="center">
					<tr>
						<td></td>
						<td>
							<h1>Alta de periodos</h1>
						</td>
					</tr>
					<tr>
						<?php if (@$_GET["mensaje"] == "correcto") { ?>
							<th>
								<div class="correcto">
									<p>Registro Correcto</p>
							</th>
							<td>
		</div>
		<a href="menua.php" class="btn">Regresar al menú</a>
	<?php } ?>
	</td>
	</tr>
	<tr>
		<?php if (@$_GET["mensaje"] == "error") { ?>
			<th>
				<div class="error">
					<p>Registro erroneo</p>
			</th>
			<td>
				</div>
				<p>Por favor revisa los campos</p>
			<?php } ?>
			</td>
	</tr>
	<tr>
		<?php if (@$_GET["mensaje"] == "date") { ?>
			<th>
				<div class="error">
					<p>Revisa las fechas</p>
			</th>
			<td>
				</div>
				<p>Por favor revisa las fechas</p>
			<?php } ?>
			</td>
	</tr>
	<tr>
		<th>Periodo: </th>
		<td><input type="text" name="periodo" maxlength="6" pattern="[0-9-]{5,}" title="Formato es año, guión, 1 o 2" required placeholder="Formato ejem= 2018-1"></td>
	</tr>
	<tr>
		<th>Fecha Inicio: </th>
		<td><input type="date" name="inicio" minlength="1" required placeholder="..."></td>
	</tr>
	<tr>
		<th>Fecha Final: </th>
		<td><input type="date" name="final" minlength="1" required placeholder="..."></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="btn" minlength="1" value="Entrar"></td>
	</tr>
	</table>
	</form>
	</div>
	</section>
	<div class="formulario">
		<h1>Visualización y modificación: </h1>
		<label for="caja_busqueda">Buscar </label>
		<input type="text" name="caja_busqueda" id="caja_busqueda">
	</div>
	<section class="principal">
		<div id="datos"></div>
	</section>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jsperiodos.js"></script>
</body>

</html>