<?php
require("validaradmin.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Alta de grupos</title>
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<meta charset="utf-8">
	<style type="text/css">

		input[type=text], select {
    width: 400px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=password], select {
    width: 400px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}
	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
	
</head>
<body>

<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesión </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta</a>
			      <a href="cambiosgrupos.php">Modificar</a>
			    </div>
			 </li>
			
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta</a>
			      <a href="cambiosmaterias.php">Modificar</a>
			    </div>
			 </li>
			 	<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta</a>
			      <a href="cambiosprofe.php">Cambios</a>
			     </div>
			 </li>	
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Jefes</a>
   				 <div class="dropdown-content">
			      <a href="altajefe.php">Alta</a>
			      <a href="cambiosjefe.php">Cambios</a>
			    </div>
			 </li>
			  <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Periodos</a>
   				 <div class="dropdown-content">
			     <a href="altaperiodo.php">Alta</a>
			      <a href="cambiosperiodos.php">Modificar</a>
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Divisiones</a>
   				 <div class="dropdown-content">
			     <a href="altadivision.php">Alta</a>
			      <a href="cambiosdivisiones.php">Modificar</a>
			      
			    </div>
			 </li>
			  <li>
				<a href="menua.php">Inicio</a>
			</li>
		</ul>
	</nav>

</header>


<section class="contenido wrapper">
	<div class="formularios">
		<form name="form" id="from" action="conealtagrupos.php" method="post">
			<table border="0" align="center">
				<tr>
					<?php if(@$_GET["m"]=="correcto"){ ?>
				<th><div class="correcto">
							<p>Registro Correcto</p> </th>
				<td>
						</div>
						<button class="btn"><a href="menua.php">Regresar al menú</a></button>
						<?php } ?>
				</td>
				</tr>
				<form action="#">
				<tr>
					<?php if(@$_GET["m"]=="error"){ ?>
				<th><div class="error">
							<p>Registro erroneo</p> </th>
				<td>
						</div>
						<p>Por favor revisa los campos</p>
						<?php } ?>
				</td>
				</tr>
				<tr>
					<th> División: </th>
					<td>
				<select name="division" width="400px">
					<?php 
						require('../conectar.php');
						$sql="select * from carreras";
						$ejecuta=mysqli_query($conexion, $sql);
						while ($datos=mysqli_fetch_array($ejecuta)) {
							
							echo " <option value=\"".$datos[2]."\">".$datos[2]."</option>\n";
						
						}

						


						 ?>
				</select>	
				</td>			
				</tr>
				<?php
				 $ejem=$_POST['division']; 
				echo $ejem; 
				?>
				</form>
				<tr>
					<th>Profesor: </th>
					<td>
				<select name="profesor" width="400px">
					<?php 
						require('../conectar.php');
						$sql="select * from profesores";
						$ejecuta=mysqli_query($conexion, $sql);
						while ($datos=mysqli_fetch_array($ejecuta)) {
							
							echo " <option value=\"".$datos[2]."\">".$datos[2]."</option>\n";
						}
						 ?>
				</select>	
				</td>			
				</tr>
				<tr>
					<th>Semestre: </th>
					<td>
				<select class="semestre" name="semestre" width="400px">
					<?php 
						require('../conectar.php');
						$sql="select * from semestres";
						$ejecuta=mysqli_query($conexion, $sql);
						while ($datos=mysqli_fetch_array($ejecuta)) {
							
							echo " <option value=\"".$datos[1]."\">".$datos[1]."</option>\n";
						}
						 ?>
				</select>	
				</td>			
				</tr>
				<tr>
					<th>Materia: </th>
					<td>
				<select class="materia" name="division" width="400px">
					<?php 
						require('../conectar.php');
						$sql="select * from materias";
						$ejecuta=mysqli_query($conexion, $sql);
						while ($datos=mysqli_fetch_array($ejecuta)) {
							
							echo " <option value=\"".$datos[2]."\">".$datos[2]."</option>\n";
						}
						 ?>
				</select>	
				</td>			
				</tr>
				<tr>
					<th> Grupo: </th>
					<td>
				<select class="grupo" name="division" width="400px">
					<?php 
						require('../conectar.php');
						$sql="select * from grupos";
						$ejecuta=mysqli_query($conexion, $sql);
						while ($datos=mysqli_fetch_array($ejecuta)) {
							
							echo " <option value=\"".$datos[1]."\">".$datos[1]."</option>\n";
						}
						 ?>
				</select>	
				</td>			
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" class="btn" minlength="1" value="Añadir"></td>
				</tr>

			</table>
		</form>
</section>


</body>
</html>	