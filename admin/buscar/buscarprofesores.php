<?php
    require ("../../conectar.php");
    $salida = "";

    $query="SELECT j.matricula, j.nombre, j.apaterno, j.amaterno, j.sexo, j.telefono, u.correo, c.cvecarrera, c.carrera, u.usuario, u.password 
                FROM profesores j 
                INNER JOIN carreras c ON j.idcarrera=c.idcarrera 
                INNER JOIN usuarios u ON j.idusuario=u.idusuario 
                WHERE j.matricula NOT LIKE '' ORDER By j.matricula";

    if (isset($_POST['consulta'])) {
        $q = $conexion->real_escape_string($_POST['consulta']);
        $query="SELECT j.matricula, j.nombre, j.apaterno, j.amaterno, j.sexo, j.telefono, u.correo, c.cvecarrera, c.carrera, u.usuario, u.password 
                    FROM profesores j 
                    INNER JOIN carreras c ON j.idcarrera=c.idcarrera 
                    INNER JOIN usuarios u ON j.idusuario=u.idusuario 
                    WHERE j.matricula LIKE '%$q%' OR j.nombre LIKE '%$q%' OR j.apaterno 
                        LIKE '%$q%' OR j.amaterno LIKE '%$q%' OR j.sexo LIKE '%$q%' OR j.telefono 
                        LIKE '%$q%' OR u.correo LIKE '%$q%' OR c.cvecarrera LIKE '%$q%' OR c.carrera 
                        LIKE '%$q%' OR u.usuario LIKE '%$q%' OR u.password LIKE '%$q%'";
    }

    $resultado = $conexion->query($query);

    if ($resultado->num_rows>0) {
        $salida.="<table border=1 class='tabla_datos'>
                <thead>
                    <tr id='titulo'>
                        <th>Matrícula</th>
                        <th>Nombre(s)</th>
                        <th>A. Paterno</th>
                        <th>A. Materno</th>
                        <th>Carrera</th>
                        <th>Sexo</th>
                        <th>Telefono</th>
                        <th>Correo</th>
                        <th>Usuario</th>
                        <th>Contraseña</th>
                        <th>Eliminar</th>
                        <th>Editar</th>
                    </tr>

                </thead>
                

        <tbody>";

        while ($datos = $resultado->fetch_assoc()) {
            $salida.="<tr>
                        <td>".$datos['matricula']."</td>
                        <td>".$datos['nombre']."</td>
                        <td>".$datos['apaterno']."</td>
                        <td>".$datos['amaterno']."</td>
                        <td>".$datos['carrera']."</td>
                        <td>".$datos['sexo']."</td>
                        <td>".$datos['telefono']."</td>
                        <td>".$datos['correo']."</td>
                        <td>".$datos['usuario']."</td>
                        <td>".$datos['password']."</td>
                        <td> <a href='#' onclick='confirmar(".$datos['matricula'].")'> 
                            <img src='../img/eliminar.png' width='50px' align='center'> </a> 
                        </td>
                        <td>
                            <a href='actualizaprofe.php?matricula=".$datos['matricula']."&nombre=".$datos['nombre']."&apaterno=".$datos['apaterno']."&amaterno=".$datos['amaterno']."&cr=".$datos['carrera']."&sx=".$datos['sexo']."&tel=".$datos['telefono']."&cor=".$datos['correo']."&usu=".$datos['usuario']."&pass=".$datos['password']."'>
                                <img src='../img/editar.png' width='50px' align='center'>
                            </a>
                        </td>
                        </tr>";

        }
        $salida.="</tbody></table>";
    }else{
         $salida.="<table border=1 class='tabla_datos'>
                        <thead>
                            <tr id='titulo'>
                                <td>No hay datos</td>
                            </tr>
                        </thead>
                        <tbody>";

    }

    echo $salida;

    $conexion->close();



?>