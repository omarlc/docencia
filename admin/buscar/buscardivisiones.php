<?php
   
    require ("../../conectar.php");

    $salida = "";

    $query = "SELECT * FROM carreras WHERE carrera NOT LIKE '' ORDER By cvecarrera LIMIT 25";

    if (isset($_POST['consulta'])) {
        $q = $conexion->real_escape_string($_POST['consulta']);
        $query = "SELECT * FROM carreras WHERE cvecarrera LIKE '%$q%' OR carrera LIKE '%$q%' OR idcarrera LIKE '%$q%'";
    }

    $resultado = $conexion->query($query);

    if ($resultado->num_rows>0) {
        $salida.="<table border=1 class='tabla_datos'>
                    <thead>
                        <tr id='titulo'>
                            <td>Clave Carrera</td>
                            <td>Nombre Carrera</td>
                            <td>Eliminar</td>
                            <td>Editar</td>
                        </tr>

                    </thead>
                    <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                        <td>".
                            $fila['cvecarrera'].
                        "</td>
                        <td>".$fila['carrera']."</td>
                        <td> 
                            <a href='#' onclick='confirmar(".$fila['idcarrera'].",\"".$fila['carrera']."\")'> 
                                <img src='../img/eliminar.png' width='50px' align='center'> 
                            </a> 
                        </td>
                        <td>
                            <a href='actualizacarrera.php?id_carrera=".$fila['idcarrera']."&clave_carrera=".$fila['cvecarrera']."&nombre_carrera=".$fila['carrera']."'>
                                <img src='../img/editar.png' width='50px' align='center'>
                            </a>
                        
                    </tr>";

        }
            $salida.="</tbody></table>";
    }else{
         $salida.="<table border=1 class='tabla_datos'>
                        <thead>
                            <tr id='titulo'>
                                <td>No hay datos</td>   
                            </tr>
                        </thead>
                    <tbody>";

    }

    echo $salida;

    $conexion->close();
?>