<?php
    require ("../../conectar.php");

    $salida = "";

    $query = "SELECT * FROM actividades WHERE nombre NOT LIKE '' ORDER By fecha";

    if (isset($_POST['consulta'])) {
        $q = $conexion->real_escape_string($_POST['consulta']);
        $query = "SELECT * FROM actividades WHERE nombre LIKE '%$q%' OR tipo LIKE '%$q%' OR fecha LIKE '%$q%' ";
    }

    $resultado = $conexion->query($query);

    if ($resultado->num_rows>0) {
        $salida.="<table border=1 class='tabla_datos'>
                    <thead>
                        <tr id='titulo'>
                            <td>nombre</td>
                            <td>tipo</td>
                            <td>fecha</td>
                            <td>Eliminar</td>
                            <td>Editar</td>
                        </tr>
                    </thead>
                    <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                        <td>".$fila['nombre']."</td>
                        <td>".$fila['tipo']."</td>
                        <td>".$fila['fecha']."</td>
                        <td> 
                            <a href='#' onclick='confirmar(".$fila['idactividad'].",\"".$fila["nombre"]."\")'> 
                                <img src='../img/eliminar.png' width='50px' align='center'> 
                            </a> 
                        </td>
                        <td>
                            <a href='actualizaarchivo.php?nombre_archivo=".$fila['nombre']."&tipo_archivo=".$fila['tipo']."&fecha_archivo=".$fila['fecha']."'>
                                <img src='../img/editar.png' width='50px' align='center'>
                            </a>
                        </td>
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
         $salida.="<table border=1 class='tabla_datos'>
                <thead>
                    <tr id='titulo'>
                        <td>No hay datos</td>
                    </tr>
                </thead>
                <tbody>";

    }
    echo $salida;

    $conexion->close();
?>