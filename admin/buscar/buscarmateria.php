<?php
    require ("../../conectar.php");
    $salida = "";

    $query = "SELECT * FROM materias WHERE materia NOT LIKE '' ORDER By materia";

    if (isset($_POST['consulta'])) {
        $q = $conexion->real_escape_string($_POST['consulta']);
        $query = "SELECT * FROM materias WHERE cvemateria LIKE '%$q%' OR materia LIKE '%$q%' OR competencias LIKE '%$q%' ";
    }

    $resultado = $conexion->query($query);

    if ($resultado->num_rows>0) {
        $salida.="<table border=1 class='tabla_datos'>
                <thead>
                    <tr id='titulo'>
                        <td>cvemateria</td>
                        <td>materia</td>
                        <td>competencias</td>
                        <td>semestre</td>
                        <td>Eliminar</td>
                        <td>Editar</td>
                    </tr>
                </thead>
                <tbody>";

        while ($fila = $resultado->fetch_assoc()) {
            $salida.="<tr>
                        <td>".$fila['cvemateria']."</td>
                        <td>".$fila['materia']."</td>
                        <td>".$fila['competencias']."</td>
                        <td>".$fila['idsemestre']."</td> 
                        <td> 
                            <a href='#' onclick='confirmar(".$fila['idmateria'].",\"".$fila['materia']."\")'> 
                                <img src='../img/eliminar.png' width='50px' align='center'> 
                            </a>  
                        </td>
                        <td>
                            <a href='actualizamateria.php?id_materia=".$fila['idmateria']."&cve_materia=".$fila['cvemateria']."&nombre_materia=".$fila['materia']."'>
                                <img src='../img/editar.png' width='50px' align='center'>
                            </a>
                        </td>
                    </tr>";

        }
        $salida.="</tbody></table>";
    }else{
         $salida.="<table border=1 class='tabla_datos'>
                        <thead>
                            <tr id='titulo'>
                                <td>No hay datos</td>                      
                            </tr>
                        </thead>
                        <tbody>";

    }
    echo $salida;

    $conexion->close();



?>