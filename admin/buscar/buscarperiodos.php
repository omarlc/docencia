<?php

    require ("../../conectar.php");
    $salida = "";

    $query = "SELECT * FROM periodos WHERE periodo NOT LIKE '' ORDER By periodo";

    if (isset($_POST['consulta'])) {
        $q = $conexion->real_escape_string($_POST['consulta']);
        $query = "SELECT * FROM periodos WHERE fechai LIKE '%$q%' OR fechaf LIKE '%$q%' OR periodo LIKE '%$q%'";
    }

    $resultado = $conexion->query($query);

    if ($resultado->num_rows>0) {

        $salida.="<table border=1 class='tabla_datos'>
                <thead>
                    <tr id='titulo'>
                        <td>Periodo</td>
                        <td>Fecha Inicio</td>
                        <td>Fecha Final</td>
                        <td>Eliminar</td>
                        <td>Editar</td>                        
                    </tr>
                </thead>
                <tbody>";

        while ($fila = $resultado->fetch_assoc()) {

            $salida.="<tr>
                        <td>".$fila['periodo']."</td>
                        <td>".$fila['fechai']."</td>
                        <td>".$fila['fechaf']."</td>
                        <td> <a href='#' onclick='confirmar(".$fila['idperiodo'].",\"".$fila['periodo']."\")'> 
                                <img src='../img/eliminar.png' width='50px' align='center'> 
                            </a> 
                        </td>
                        <td>
                            <a href='actualizarperiodo.php?id_periodo=".$fila['idperiodo']."&periodo=".$fila['periodo'].
                                                                        "&fecha_incio=".$fila['fechai']."&fecha_final=".$fila['fechaf']."'>
                                <img src='../img/editar.png' width='50px' align='center'>
                            </a>
                        </td>
                    </tr>";
        }
        $salida.="</tbody></table>";
    }else{
         $salida.="<table border=1 class='tabla_datos'>
                        <thead>
                            <tr id='titulo'>
                                <td>No hay datos</td>    
                            </tr>
                        </thead>
                        <tbody>";
    }

    echo $salida;

    $conexion->close();



?>