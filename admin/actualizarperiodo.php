<?php
$idperiodo=($_GET['id_periodo']);
$periodo=($_GET['periodo']);
$fechai=$_GET['fecha_incio'];
$fechaf=$_GET['fecha_final'];
?>
<html>
	<head>
		<title>Formulario</title>
		<meta charset="utf-8">
		<link rel="stylesheet" type="text/css" href="../css/estilos.css">
		<link rel="icon" type="image/png" href="../img/tesci.ico">
		
		<style type="text/css">

			input[type=text], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
			}
			input[type=date], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
			}
			input[type=password], select {
				width: 300px;
				padding: 12px 20px;
				margin: 8px 0;
				display: inline-block;
				border: 1px solid #ccc;
				border-radius: 4px;
				box-sizing: border-box;
			}
			a{
				text-decoration: none;
				color: white;
			}
			a:hover{
				color: black;
			}

			.correcto{
				border-radius: 5px;
				background-color: green;
				color: white;
				margin: auto;
				height: 20px;
				width: 150px;
			}
		</style>
	</head>
	<body>
		<div class="headerlogopag">
		<img src="../img/logo.png">
		<header>
			<nav class="menu">

				<ul>
					<li>
						<a href="../cerrarsesion.php"> Cerrar Sesión </a>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Actividades</a>
						<div class="dropdown-content">
						<a href="altaarchivos.php">Alta</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Grupos</a>
						<div class="dropdown-content">
						<a href="altagrupos.php">Alta</a>
						
						</div>
					</li>
					
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Materias</a>
						<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>
						
						</div>
					</li>
						<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Profesores</a>
						<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						
						</div>
					</li>	
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Jefes</a>
						<div class="dropdown-content">
						<a href="altajefe.php">Alta y cambios</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Periodos</a>
						<div class="dropdown-content">
						<a href="altaperiodo.php">Alta y cambios</a>
						
						</div>
					</li>
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropbtn">Divisiones</a>
						<div class="dropdown-content">
						<a href="altadivision.php">Alta y cambios</a>
							
						</div>
					</li>
					<li>
						<a href="menua.php">Inicio</a>
					</li>
				</ul>
			</nav>

		</header>

		<section class="contenido wrapper">
			<div class="formularios">
				<table border="0" align="center">
					<form name="form1" id="form1" method="post" action="actualizarperiodo2.php">
						<h1>Actualizar</h1>
						<tr>
							<th>Periodo: </th>
							<td>
								<input type="text" name="periodo" minlength="6" title="Formato es año, guión, 1 o 2" required value="<?php echo $periodo; ?>" placeholder="Formato ejem= 2018-1"><br>
							</td>
						</tr>
						<tr>
							<th>Fecha Inicio: </th>
							<td>
								<input type="date" name="fechai" id="fechai" value="<?php echo $fechai; ?>"><br>
							</td>
						</tr>
						<tr>
							<th>Fecha Termino: </th> 
							<td>
								<input type="date" name="fechaf" id="fechaf" value="<?php echo $fechaf; ?>"><br>
							</td>
						</tr>
						<th> 
							<td>
								<input type="text" name="idperiodo" style="visibility:hidden" id="idperiodo" value="<?php echo $idperiodo; ?>"><br>
							</td>
						</th>
						</tr>
						<tr>
							<th><input type="submit" class="btn" value="Actualizar"> </th>
							<td>
								<a href="altaperiodo.php">
								<input type="button" class="btn" value="Cancelar">
								</a>
							</td>
						</tr>
					</form>
				</table>
			</div>
		</section>
	</body>
</html>