<?php
require("validaradmin.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Alta de archivos</title>
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<meta charset="utf-8">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}


		input[type=date],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}

		a {
			text-decoration: none;
			color: white;
		}

		a:hover {
			color: black;
		}

		.correcto {
			border-radius: 5px;
			background-color: green;
			color: white;
			margin: auto;
			height: 20px;
			width: 150px;
		}

		.error {
			border-radius: 5px;
			background-color: red;
			color: white;
			margin: auto;
			height: 20px;
			width: 150px;
		}
	</style>
	<script>
		function confirmar(id_actividad, nombre_actividad) {
			if (confirm("¿Deseas eliminar la actividad: " + nombre_actividad + "?")) {
				window.location.href = "eliminararchivo.php?id_actividad=" + id_actividad;
			}
		}
	</script>
</head>

<body>
	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesión </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Alta</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta</a>

					</div>
				</li>

				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Jefes</a>
					<div class="dropdown-content">
						<a href="altajefe.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Periodos</a>
					<div class="dropdown-content">
						<a href="altaperiodo.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Divisiones</a>
					<div class="dropdown-content">
						<a href="altadivision.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menua.php">Inicio</a>
				</li>
			</ul>
		</nav>

	</header>

	<section class="contenido wrapper">
		<div class="formularios">
			<form name="form" id="from" action="conealtaarchivos.php" method="post">
				<table border="0" align="center">
					<tr>
						<td></td>
						<td>
							<h1>Alta de actividades</h1>
						</td>
					</tr>
					<tr>
						<?php if (@$_GET["mensaje"] == "correcto") { ?>
							<th>
								<div class="correcto">
									<p>Registro Correcto</p>
							</th>
							<td>
		</div>
		<a href="menua.php"><button class="btn">Regresar al menú</a></button>
	<?php } ?>
	</td>
	</tr>
	<tr>
		<?php if (@$_GET["mensaje"] == "error") { ?>
			<th>
				<div class="error">
					<p>Registro erroneo</p>
			</th>
			<td>
				</div>
				<p>Por favor revisa los campos</p>
			<?php } ?>
			</td>
	</tr>
	<tr>
		<th> Periodo: </th>
		<td>
			<select class="periodo" name="periodo" width="400px">
				<?php
				require('../conectar.php');
				$sql = "select * from periodos";
				$ejecuta = mysqli_query($conexion, $sql);
				while ($datos = mysqli_fetch_array($ejecuta)) {

					echo " <option value=\"" . $datos[3] . "\">" . $datos[3] . "</option>\n";
				}
				?>
			</select>
		</td>
	</tr>
	<tr>
		<th>Actividad: </th>
		<td><input type="text" name="nombre" required placeholder="Intoduce el nombre de la actividad..."></td>
	</tr>
	<tr>
		<th>Tipo: </th>

		<td>
			<select name="tipo" requiered placeholder="Selecciona el tipo de actividad...">
				<option>Inicio</option>
				<option>Seguimiento</option>
				<option>Final</option>
			</select>
		</td>
	</tr>
	<tr>
		<th>Fecha Entrega: </th>
		<td><input type="date" name="fecha" minlength="1" required placeholder="..."></td>
	</tr>
	<tr>
		<td></td>
		<td><input type="submit" class="btn" minlength="1" value="Entrar"></td>
	</tr>
	</table>
	</form>
	</div>
	</section>

	<div class="formulario">
		<h1>Visualización y modificación: </h1>
		<label for="caja_busqueda">Buscar </label>
		<input type="text" name="caja_busqueda" id="caja_busqueda">
	</div>

	<section class="principal">
		<div id="datos"></div>
	</section>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jsarchivos.js"></script>

</body>

</html>