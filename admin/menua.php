<?php
require("validaradmin.php");
?>

<!DOCTYPE html>
<html>
<head>
	<title>Bienvenido administrador</title>
	<!--Link del tipo de fuente-->
	<link href="https://fonts.googleapis.com/css?family=Montserrat|Questrial" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<meta charset="utf-8">
	
</head>
<body>

<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesión </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Alta</a>
			      
			    </div>
			 </li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta</a>
			      
			    </div>
			 </li>
			
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 	<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>
			      
			     </div>
			 </li>	
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Jefes</a>
   				 <div class="dropdown-content">
			      <a href="altajefe.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Periodos</a>
   				 <div class="dropdown-content">
			     <a href="altaperiodo.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Divisiones</a>
   				 <div class="dropdown-content">
			     <a href="altadivision.php">Alta y cambios</a>
			           
			    </div>
			 </li>
			  <li>
				<a href="menua.php">Inicio</a>
			</li>
		</ul>
	</nav>

</header>


<section class="contenido wrapper">
	<div class="mbienvenida">
		<center>
			<h1>Bienvenido</h1>
			<h2><i>Administrador</i></h2>
			<img src="../img/man.png" width="100px">
			<img src="../img/wman.png" width="100px">
			
		</center>
	</div>
	
	
</section>


</body>
</html>