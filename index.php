<!DOCTYPE html>
<html>

<head>
	<title>Bienvenidos</title>
	<link rel="stylesheet" type="text/css" href="css/estilos.css">
	<link rel="icon" type="image/png" href="img/tesci.ico">
	<meta charset="utf-8">
</head>

<body>

	<div class="headerlogo">
		<img src="img/logo.png">
	</div>



	<div class="form">
		<form name="from1" id="from1" action="conelogin.php" method="post">
			<table border="0" align="center">
				<tr>
					<th>Usuario: </th>
					<td><input type="text" name="frmUsuario" id="frmUsuario" required placeholder="Introduce tu Usuario..." autofocus></td>
				</tr>

				<tr>
					<th>Password: </th>
					<td><input type="Password" name="frmPassword" id="frmPassword" required minlength="8" maxlength="16" placeholder="Introduce tu Contraseña..."></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="submit" class="btn" value="Entrar"></td>
				</tr>

				<?php if (@$_GET["m"] == "error") { ?>
					<div class="error">
						<p>Verifica Usuario y/o contraseña</p>
					</div>
				<?php } ?>


				<tr>
					<td></td>
					<td rowspan="2"> <a href="contrasena.php"> Olvide mi contraseña</a></td>
				</tr>

			</table>
		</form>
	</div>
</body>

</html>