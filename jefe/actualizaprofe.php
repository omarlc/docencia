<?php
$matricula=($_GET['m']);
$nombre=$_GET['nom'];
$paterno=$_GET['ap'];
$materno=$_GET['am'];
$sexo=$_GET['sx'];
$telefono=$_GET['tel'];
$correo=$_GET['cor'];
$usu=$_GET['usu'];
$pass=$_GET['pass'];
?>

<html>
<head>
	<title>Formulario</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<style type="text/css">

		input[type=text], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=password], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}

	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
</head>
<body>
	<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesion </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>      
			      <a href="asignar.php">Asignar grupos y materias</a>
			      <a href="visualizacion.php">Actividades</a>
			    </div>
			 </li>		
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Visualización</a>
			      
			    </div>
			 </li>	
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li>
				<a href="menub.php">Inicio</a>
			</li>
		</ul>
	</nav>
</header>




<section class="contenido wrapper">
	<div class="formularios">
				<table border="0" align="center">
	<h1>Actualizar</h1>

		<form name="form1" id="form1" method="post" action="actualizaprofe2.php">

		<tr>
			<th>Matricula: </th>
			<td><input type="text" name="matricula" id="matricula" value="<?php echo $matricula; ?>"><br></td>
		</tr>
		 
		<tr>
			<th>Nombre (s): </th>
			<td><input type="text" name="nombre" id="nombre" value="<?php echo $nombre; ?>"><br></td>
		</tr>
		<tr>
			<th>Paterno: </th> 
			<td><input type="text" name="apaterno" id="apaterno" value="<?php echo $paterno; ?>"><br></td>
		</tr>

		<tr>
			<th>Materno: </th>  
			<td><input type="text" name="amaterno" id="amaterno" value="<?php echo $materno; ?>"><br></td>
		</tr>	
		<tr>
					<th>Sexo :</th>

					<td><select id="sexo" name="sexo" requiered  placeholder="Selecciona tu sexo..."> 
						<optgroup>
							<option></option>
							<option>Masculino</option>
							<option>Femenino</option>
						</optgroup>
					</select></td>
				</tr>
		 <tr>
			<th>Telefono: </th> 
			<td><input type="text" name="telefono" id="telefono" value="<?php echo $telefono; ?>"><br></td>
		 </tr>
		 <tr>
			<th>Correo: </th> 
			<td><input type="text" name="correo" id="correo" value="<?php echo $correo; ?>"><br></td>
		 </tr>
		 <tr>
			<th>Nombre de Usuario: </th>
			<td><input type="text" name="usuario" id="usuario" value="<?php echo $usu; ?>"><br></td>
		</tr>
		 <tr>
			<th>Contraseña: </th>
		 		<td><input type="text" name="password" id="password" value="<?php echo $pass; ?>"><br></td>
		</tr>
		
		<tr>
			<th><input type="submit" class="btn" value="Actualizar"> </th>
		
		<td><a href="altaprofesores.php">
		<input type="button" class="btn" value="Cancelar">
		</a></td>
		</tr>
		</form>
		</table>
	</div>
	</section>


</body>
</html>