<?php
require("validarjefe.php");
require("../conectar.php");

$user = $_SESSION['user'];

$sql = "SELECT * FROM usuarios WHERE usuario = '$user'";
$ejecuta = mysqli_query($conexion, $sql);
while ($datos = mysqli_fetch_array($ejecuta)) {
	$idusuario = $datos[0];
}

$sql1 = "SELECT * FROM jefes WHERE idusuario = '$idusuario'";
$ejecuta1 = mysqli_query($conexion, $sql1);
while ($datos1 = mysqli_fetch_array($ejecuta1)) {
	$idcar = $datos1['idcarrera'];
	$nombre = $datos1['nombre'];
	$apa = $datos1['apaterno'];
	$ama = $datos1['amaterno'];
	$sexo = $datos1['sexo'];
}

$sql2 = "SELECT * FROM carreras WHERE idcarrera = '$idcar'";
$ejecuta2 = mysqli_query($conexion, $sql2);
while ($datos2 = mysqli_fetch_array($ejecuta2)) {
	$carr = $datos2['carrera'];
}
?>

<!DOCTYPE html>
<html>

<head>
	<?php
	echo "<title>" . "Bienvenido" . " " . $nombre . " " . $apa . " " . $ama . "</title>";
	?>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<meta charset="utf-8">
</head>

<body>

	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">
			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesion </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						<a href="asignar.php">Asignar grupos y materias</a>
						<a href="visualizacion.php">Actividades</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Visualización</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta y cambios</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>
					</div>
				</li>
				<li>
					<a href="menub.php">Inicio</a>
				</li>
			</ul>
		</nav>
	</header>

	<section class="contenido wrapper">
		<div class="mbienvenida">
			<center>
				<h1>Bienvenido</h1>
				<?php
				echo "<h2><i>" . $nombre . " " . $apa . " " . $ama . " </i></h2>";
				?>
				<?php
				if ($sexo == "Masculino") {
					echo "<img src=" . "../img/man.png" . " width=" . "100px" . ">";
				} elseif ($sexo == "Femenino") {
					echo "<img src=" . "../img/wman.png" . " width=" . "100px" . ">";
				}
				?>
			</center>
		</div>
	</section>

</body>

</html>