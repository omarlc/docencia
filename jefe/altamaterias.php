<?php
require("validarjefe.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Alta de materias</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta charset="utf-8">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}
	</style>
	<script type="text/javascript">
		function confirmar(x) {
			if (confirm("Deseas eliminar el registro con ID: " + x + "?")) {
				window.location.href = "eliminarmateria.php?m=" + x;
			}
		}
	</script>

</head>

<body>

	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesion </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						<a href="asignar.php">Asignar grupos y materias</a>
						<a href="visualizacion.php">Actividades</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Visualización</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menub.php">Inicio</a>
				</li>
			</ul>
		</nav>
	</header>


	<section class="contenido wrapper">
		<div class="formularios">
			<form name="form" id="form" action="conealtamaterias.php" method="post">
				<table border="0" align="center">
					<tr>
						<td></td>
						<td>
							<h1>Alta de materias</h1>
						</td>
					</tr>
					<?php if (@$_GET["m"] == "correcto") { ?>
						<tr>
							<td></td>
							<th>
								<div class="correcto">
									<p>Registro Correcto</p>
								</div>
							</th>
						</tr>
					<?php } ?>
					<?php if (@$_GET["m"] == "error") { ?>
						<tr>
							<th>
								<div class="error">
									<p>Registro erroneo</p>
									<p>Por favor revisa los campos</p>
								</div>
							</th>
						</tr>
					<?php } ?>

					<tr>
						<th>Clave materia: </th>
						<td><input type="text" name="cve" maxlength="10" required placeholder="Introduce la clave de la materia..."></td>
					</tr>
					<tr>
						<th>Nombre materia: </th>
						<td><input type="text" name="name" required placeholder="Introduce el nombre de la materia..."></td>
					</tr>
					<tr>
						<th>Semestre: </th>
						<td>
							<select class="division" name="semestre" width="400px">
								<?php
								require('../conectar.php');
								$sql = "select * from semestres";
								$ejecuta = mysqli_query($conexion, $sql);
								while ($datos = mysqli_fetch_array($ejecuta)) {
									echo " <option value=\"" . $datos[1] . "\">" . $datos[1] . "</option>\n";
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th>Número de competencias: </th>
						<td>
							<select name="comp">
								<option>1</option>
								<option>2</option>
								<option>3</option>
								<option>4</option>
								<option>5</option>
								<option>6</option>
								<option>7</option>
								<option>8</option>
								<option>9</option>
								<option>10</option>
								<option>11</option>
								<option>12</option>
								<option>13</option>
								<option>14</option>
								<option>15</option>
							</select>
						</td>
					</tr>
					<tr>
						<td></td>
						<td><input type="submit" class="btn" minlength="1" value="Entrar"></td>
					</tr>

				</table>
			</form>
	</section>

	<div class="formulario">
		<h1>Visualización y modificación: </h1>
		<label for="caja_busqueda">Buscar</label>
		<input type="text" name="caja_busqueda" id="caja_busqueda"></input>
	</div>

	<section class="principal">
		<div id="datos"></div>
	</section>

	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jsmaterias.js"></script>

</body>

</html>