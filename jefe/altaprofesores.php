<?php
require("validarjefe.php");
?>
<!DOCTYPE html>
<html>

<head>
	<title>Alta de Profesores</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta charset="utf-8">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}


		input[type=password],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}

		a {
			text-decoration: none;
			color: white;
		}

		a:hover {
			color: black;
		}

		.correcto {
			border-radius: 5px;
			background-color: green;
			color: white;
			margin: auto;
			height: 20px;
			width: 150px;
		}
	</style>
	<script type="text/javascript">
		function confirmar(x) {
			if (confirm("Deseas eliminar el registro con la Matrícula: " + x + "?")) {

				window.location.href = "eliminarprofe.php?m=" + x;
			}
		}
	</script>

</head>

<body>

	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesion </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						<a href="asignar.php">Asignar grupos y materias</a>
						<a href="visualizacion.php">Actividades</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Visualización</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menub.php">Inicio</a>
				</li>
			</ul>
		</nav>
	</header>

	<section class="contenido wrapper">
		<div class="formularios">
			<form name="form" id="from" action="conealtaprofe.php" method="post">
				<table border="0" align="center">
					<tr>
						<td></td>
						<td>
							<h1>Alta de profesores</h1>
						</td>
					</tr>
					<tr>
						<?php if (@$_GET["m"] == "campo") { ?>
							<th>
								<div class="error">
									<p>Falta campo</p>
							</th>
							<td>
		</div>
		<p>Por favor revisa los campos</p>
	<?php } ?>
	</td>
	</tr>
	<tr>
		<?php if (@$_GET["m"] == "correcto") { ?>
			<th>
				<div class="correcto">
					<p>Registro Correcto</p>
			</th>
			<td>
				</div>
				<button class="btn"><a href="menub.php">Regresar al menú</a></button>
			<?php } ?>
			</td>
	</tr>
	<tr>
		<?php if (@$_GET["m"] == "error") { ?>
			<th>
				<div class="error">
					<p>Grupo ya existente</p>
			</th>
			<td>
				</div>
				<p>Por favor revisa los campos</p>
			<?php } ?>
			</td>
	</tr>
	<tr>
		<th>Matricula de profesor: </th>
		<td><input type="text" name="clave" minlength="1" maxlength="10" pattern="[0-9]{1,}" title="Solo letras" required placeholder="Introduce tu matricula..."></td>
	</tr>
	<tr>
		<th>Nombre: </th>
		<td><input type="text" name="nombre" required minlength="1" placeholder="Introduce tu Nombre..."></td>
	</tr>
	<tr>
		<th>Apellido Paterno: </th>
		<td><input type="text" name="apaterno" required minlength="1" placeholder="Introduce tu Apellido Paterno..."></td>
	</tr>
	<tr>
		<th>Apellido Materno: </th>
		<td><input type="text" name="amaterno" required minlength="1" placeholder="Introduce tu Apellido Materno..."></td>
	</tr>
	<tr>
		<th>Sexo :</th>

		<td><select name="sexo" requiered placeholder="Selecciona tu sexo...">
				<option></option>
				<option>Masculino</option>
				<option>Femenino</option>
			</select></td>
	</tr>
	<tr>
		<th>Telefono: </th>
		<td><input type="text" name="telefono" maxlength="10" minlength="1" required placeholder="Introduce tu telefono..."></td>
	</tr>
	<tr>
		<th>Usuario: </th>
		<td><input type="text" name="usuario" id="frmUsuario" minlength="1" maxlength="16" required placeholder="Introduce tu Usuario..."></td>
	</tr>

	<tr>
		<th>Password: </th>
		<td><input type="text" name="password" id="frmPassword" required minlength="8" maxlength="16" placeholder="Introduce tu Contraseña..."></td>
	</tr>
	<tr>
		<th>Correo electronico: </th>
		<td><input type="text" name="correo" id="correo" minlength="8" required placeholder="Introduce tu correo electronico..."></td>
	</tr>
	<tr>
		<center>
			<td></td>
			<td><input type="submit" class="btn" minlength="1" value="Entrar"></td>
		</center>
	</tr>

	</table>
	</form>
	</div>


	</section>

	<div class="formulario">
		<h1>Visualización y modificación: </h1>
		<label for="caja_busqueda">Buscar </label>
		<input type="text" name="caja_busqueda" id="caja_busqueda"></input>
	</div>

	<section class="principal">



		<div id="datos"></div>


	</section>



	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jsprofesores.js"></script>


</body>

</html>