<?php
require("validarjefe.php");
require("../conectar.php");
$idc=$_GET['idc'];
$idp=$_GET['idp'];

$_SESSION['idc']=$idc;
$_SESSION['idp']=$idp;

$sql="SELECT * from profesores WHERE idprof='$idp'";
    $ejecuta=mysqli_query($conexion, $sql);
    while ($datos=mysqli_fetch_array($ejecuta)){
        $nombre=$datos['nombre'];
        $apa=$datos['apaterno'];

        }
        $sql="SELECT * from carreras WHERE idcarrera='$idc'";
    $ejecuta=mysqli_query($conexion, $sql);
    while ($datos=mysqli_fetch_array($ejecuta)){
        $carrera=$datos['carrera'];
        }

?>
<!DOCTYPE html>
<html>
<head>
	<title>Alta de grupos</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta charset="utf-8">
	<style type="text/css">

		input[type=text], select {
    width: 400px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=password], select {
    width: 400px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}
	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
	
</head>
<body>

<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesion </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>      
			      <a href="asignar.php">Asignar grupos y materias</a>
			      <a href="visualizacion.php">Actividades</a>
			    </div>
			 </li>		
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Visualización</a>
			      
			    </div>
			 </li>	
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li>
				<a href="menub.php">Inicio</a>
			</li>
		</ul>
	</nav>
</header>
<section class="contenido wrapper">
	<?php if(@$_GET["m"]=="correcto"){ ?>
				<div class="correcto">
					<p>Correcto!</p> 
				</div>
				<?php } ?>
	<?php if(@$_GET["m"]=="error"){ ?>
				<div class="error">
					<p>Algo salio mal</p> 
				</div>
				<?php } ?>


	<div class="formulario">
		<h1>
		<?php 		echo "Selecciono a $nombre $apa de la división $carrera ";  ?></h1><br>
						<h1>Seleccionar grupo: </h1>
						<label for="caja_busqueda">Buscar </label>
						<input type="text" name="caja_busqueda" id="caja_busqueda"></input>
						<a href="asignar.php" ><input type="button" class="btn" minlength="1" value="Regresar"></a>
					</div>
				
	<section class="principal">

	

	<div id="datos"></div>
	
	
</section>



<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="js/jsasignargrupo.js"></script>
<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
	
</section>


</body>
</html>	