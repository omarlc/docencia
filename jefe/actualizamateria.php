<?php
$cvemateria = ($_GET['m']);
$materia = ($_GET['c']);
$idmateria = ($_GET['i']);
$s = ($_GET['s']);

?>

<html>

<head>
	<title>Actualizar Materia</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}
	</style>
</head>

<body>
	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesion </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						<a href="asignar.php">Asignar grupos y materias</a>
						<a href="visualizacion.php">Actividades</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Visualización</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menub.php">Inicio</a>
				</li>
			</ul>
		</nav>
	</header>

	<section class="contenido wrapper">
		<div class="formularios">
			<form name="form1" id="form1" method="post" action="actualizamateria2.php">
				<table border="0" align="center">
					<tr>
						<td></td>
						<td>
							<h1>Actualizar Materia</h1>
						</td>
					</tr>
					<tr>
						<th>Clave de Materia: </th>
						<td><input type="text" name="cvemateria" id="cvemateria" maxlength="10" value="<?php echo $cvemateria; ?>"><br></td>
					</tr>
					<tr>
						<th>Materia: </th>
						<td><input type="text" name="materia" id="materia" value="<?php echo $materia; ?>"><br></td>
					</tr>
					<tr>
						<th>Semestre: </th>
						<td>
							<select class="division" name="semestre" width="400px">
								<?php
								require('../conectar.php');
								$sql = "select * from semestres";
								$ejecuta = mysqli_query($conexion, $sql);
								while ($datos = mysqli_fetch_array($ejecuta)) {
									echo " <option>" . $datos[1] . "</option>\n";
								}
								?>
							</select>
						</td>
					</tr>
					<tr>
						<th></th>
						<td><input type="text" name="idmateria" style="visibility:hidden" id="idmateria" value="<?php echo $idmateria; ?>"><br></td>
					</tr>
					<tr>
						<th><input type="submit" class="btn" value="Actualizar"></th>
						<td><a href="altamaterias.php"><input type="button" class="btn" value="Cancelar"></a></td>
					</tr>
				</table>
			</form>
		</div>
	</section>
</body>

</html>