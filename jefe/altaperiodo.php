<?php
require("validarjefe.php");
?>
<!DOCTYPE html>
<html>
<head>
	<title>Alta de periodos</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<meta charset="utf-8">
	<style type="text/css">

	input[type=text], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=date], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}
	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
	
</head>
<body>

<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesion </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>      
			      <a href="asignar.php">Asignar grupos y materias</a>
			      <a href="visualizacion.php">Actividades</a>
			    </div>
			 </li>		
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Visualización</a>
			      
			    </div>
			 </li>	
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li>
				<a href="menub.php">Inicio</a>
			</li>
		</ul>
	</nav>
</header>

<section class="contenido wrapper">
	<div class="formularios">
		<form name="form" id="from" action="conealtaperiodo.php" method="post">
			<table border="0" align="center">
				<tr>
					<?php if(@$_GET["m"]=="correcto"){ ?>
				<th><div class="correcto">
							<p>Registro Correcto</p> </th>
				<td>
						</div>
						<a href="menua.php"><button class="btn">Regresar al menú</a></button>
						<?php } ?>
				</td>
				</tr>
				<tr>
					<?php if(@$_GET["m"]=="error"){ ?>
				<th><div class="error">
							<p>Registro erroneo</p> </th>
				<td>
						</div>
						<p>Por favor revisa los campos</p>
						<?php } ?>
				</td>
				</tr>
				<tr>
					<th>Periodo: </th>
					<td><input type="text" name="name" maxlength="6" pattern="[0-9-]{5,}" title="Formato es año, guión, 1 o 2" required placeholder="Formato ejem= 2018-1"></td>
				</tr>
				<tr>
					<th>Fecha Inicio: </th>
					<td><input type="date" name="inicio" minlength="1" required placeholder="..."></td>
				</tr>
				<tr>
					<th>Fecha Final: </th>
					<td><input type="date" name="final" minlength="1" required placeholder="..."></td>
				</tr>
				<tr>
					<td></td>
					<td><input type="submit" class="btn" minlength="1" value="Entrar"></td>
				</tr>

			</table>
		</form>
</section>


</body>
</html>	