<?php
require("validarjefe.php");

require("../conectar.php");
$user = $_SESSION['user'];

$sql = "SELECT * from usuarios WHERE usuario='$user'";
$ejecuta = mysqli_query($conexion, $sql);
while ($datos = mysqli_fetch_array($ejecuta)) {
	$idcarrera = $datos[0];
}
$sql1 = "SELECT * from jefes WHERE idusuario='$idcarrera'";
$ejecuta1 = mysqli_query($conexion, $sql1);
while ($datos1 = mysqli_fetch_array($ejecuta1)) {
	$idcar = $datos1['idcarrera'];
	$nombre = $datos1['nombre'];
	$apa = $datos1['apaterno'];
	$ama = $datos1['amaterno'];
	$sexo = $datos1['sexo'];
}


?>
<!DOCTYPE html>
<html>

<head>
	<title>Alta de grupos</title>
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<link rel="stylesheet" type="text/css" href="css/estilo.css">
	<meta charset="utf-8">
	<style type="text/css">
		input[type=text],
		select {
			width: 300px;
			padding: 12px 20px;
			margin: 8px 0;
			display: inline-block;
			border: 1px solid #ccc;
			border-radius: 4px;
			box-sizing: border-box;
		}
	</style>
	<script type="text/javascript">
		function confirmar(x) {
			if (confirm("Deseas reportar la actividad: " + x + "?")) {

				window.location.href = "correo/validarcorreo.php?m=" + x;
			}
		}
	</script>


</head>

<body>

	<div class="headerlogopag">
		<img src="../img/logo.png">
	</div>

	<header>
		<nav class="menu">

			<ul>
				<li>
					<a href="../cerrarsesion.php"> Cerrar Sesion </a>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Profesores</a>
					<div class="dropdown-content">
						<a href="altaprofesores.php">Alta y cambios</a>
						<a href="asignar.php">Asignar grupos y materias</a>
						<a href="visualizacion.php">Actividades</a>
					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Actividades</a>
					<div class="dropdown-content">
						<a href="altaarchivos.php">Visualización</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Grupos</a>
					<div class="dropdown-content">
						<a href="altagrupos.php">Alta y cambios</a>

					</div>
				</li>
				<li class="dropdown">
					<a href="javascript:void(0)" class="dropbtn">Materias</a>
					<div class="dropdown-content">
						<a href="altamaterias.php">Alta y cambios</a>

					</div>
				</li>
				<li>
					<a href="menub.php">Inicio</a>
				</li>
			</ul>
		</nav>
	</header>


	<section class="contenido wrapper">
		<div class="formulario">
			<?php
			if (@$_GET["m"] == "correcto") { ?>

				<div class="correcto">
					<p>Correo enviado exitosamente</p>
				</div>

			<?php } ?>
			<?php if (@$_GET["m"] == "error") { ?>

				<div class="error">
					<p>Error al enviar el correo</p>
				</div>

			<?php } ?>
			<h1>Visualización y modificación: </h1>
			<label for="caja_busqueda">Buscar </label>
			<input type="text" name="caja_busqueda" id="caja_busqueda"></input>

			<a href="pdf1/reporte.php" target="_new"><input type="button" class="btn1" minlength="1" value="Generar reporte"></a>
		</div>
	</section>

	<section class="principal">



		<div id="datos"></div>


	</section>



	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jsactividades.js"></script>

	</section>




</body>

</html>