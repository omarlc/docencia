<?php
$idperiodo=($_GET['p']);
$periodo=($_GET['m']);
$fechai=$_GET['nom'];
$fechaf=$_GET['ap'];
?>

<html>
<head>
	<title>Formulario</title>
	<meta charset="utf-8">
	<link rel="icon" type="image/png" href="../img/tesci.ico">
	<link rel="stylesheet" type="text/css" href="../css/estilos.css">
	<style type="text/css">

		input[type=text], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }


input[type=password], select {
    width: 300px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
    }

    a{
			text-decoration: none;
			color: white;
		}

	a:hover{
		color: black;
	}

	.correcto{
  border-radius: 5px;
    background-color: green;
    color: white;
    margin: auto;
    height: 20px;
    width: 150px;
}
	</style>
</head>
<body>
	<div class="headerlogopag">
	<img src="../img/logo.png">
</div>

<header>
	<nav class="menu">

		<ul>
			<li>
				<a href="../cerrarsesion.php"> Cerrar Sesion </a>
			</li>
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Profesores</a>
   				 <div class="dropdown-content">
			      <a href="altaprofesores.php">Alta y cambios</a>      
			      <a href="asignar.php">Asignar grupos y materias</a>
			      <a href="visualizacion.php">Actividades</a>
			    </div>
			 </li>		
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Actividades</a>
   				 <div class="dropdown-content">
			     <a href="altaarchivos.php">Visualización</a>
			      
			    </div>
			 </li>	
			<li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Grupos</a>
   				 <div class="dropdown-content">
			     <a href="altagrupos.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			 <li class="dropdown">
   				 <a href="javascript:void(0)" class="dropbtn">Materias</a>
   				 <div class="dropdown-content">
			     <a href="altamaterias.php">Alta y cambios</a>
			      
			    </div>
			 </li>
			  <li>
				<a href="menub.php">Inicio</a>
			</li>
		</ul>
	</nav>
</header>


<section class="contenido wrapper">
	<div class="formularios">
				<table border="0" align="center">
	<h1>Actualizar</h1>

		<form name="form1" id="form1" method="post" action="actualizarperiodo2.php">
<tr>
		
		<tr>
			<th>Matricula: </th>
			<td><input type="text" name="periodo" id="periodo" value="<?php echo $periodo; ?>"><br></td>
		</tr>
		 
		<tr>
			<th>Nombre (s): </th>
			<td><input type="date" name="fechai" id="fechai" value="<?php echo $fechai; ?>"><br></td>
		</tr>
		<tr>
			<th>Paterno: </th> 
			<td><input type="date" name="fechaf" id="fechaf" value="<?php echo $fechaf; ?>"><br></td>
		</tr>
		
		<th> 
		<td>
			<input type="text" name="idperiodo" style="visibility:hidden" id="idperiodo" value="<?php echo $idperiodo; ?>"><br></td>
		
		</th>
		</tr>
		<tr>
			<th><input type="submit" class="btn" value="Actualizar"> </th>
		
		<td><a href="cambiosperiodos.php">
		<input type="button" class="btn" value="Cancelar">
		</a></td>
		</tr>
		</form>
		</table>
	</div>
	</section>


</body>
</html>